:: Copies the correct FastMM FullDebugMode DLL from either %2 or %FastMM% to the output directory
:: parameters
:: 1="$(Platform)"
:: 2="$(FastMM)\FullDebugMode DLL\Precompiled\"
:: 3="$(OUTPUTDIR)"
:: 4="$(DEFINES)"
:: from a pre-build, pre-link or post-build event, call it like this:
:: "$(INPUTDIR)Copy_FastMM_FullDebugMode_Dll_PostBuildEvent.bat" "$(Platform)" "$(FastMM)\FullDebugMode DLL\Precompiled\" "$(OUTPUTDIR)" "$(DEFINES)"
>> %temp%\FastMM-post-build.txt echo 1=%1
>> %temp%\FastMM-post-build.txt echo 2=%2
>> %temp%\FastMM-post-build.txt echo 3=%3
>> %temp%\FastMM-post-build.txt echo 4=%4
  setlocal
  set Defines=%4

:: http://stackoverflow.com/questions/5491383/find-out-whether-an-environment-variable-contains-a-substring
  if [%defines%]==[%defines:FastMM=%] goto :NoFastMM

:: Versions of Delphi <= 2010 do not have $(Platform)
  if %1=="" goto :Win32
  if %1=="Win32" goto :Win32
  if %1=="Win64" goto :Win64

:NoCompatiblePlatform
:: Platform which does not support FullDebugMode
>> %temp%\FastMM-post-build.txt echo No FastMM FullDebugMode compatible platform in parameter 1=$(Platform)
  goto :exit

:NoFastMM
>> %temp%\FastMM-post-build.txt echo No FastMM in parameter 4=$(DEFINES)

:exit
  endlocal
  goto :eof

:do
>> %temp%\FastMM-post-build.txt echo %*
>> %temp%\FastMM-post-build.txt %*
  goto :exit

:Win32
  call :do set FastMMDLL=FastMM_FullDebugMode.dll
  goto :Windows
:Win64
  call :do set FastMMDLL=FastMM_FullDebugMode64.dll
  goto :Windows

:Windows
:: find out where the DLL is; if we cannot find it: bail out through :copyError
:: Try %2 first
  call :do set FastMMDLLPath=%2\%FastMMDLL%
  if exist %FastMMDLLPath% goto :copyOK
:: Try a path in %FastMM%
  call :do set FastMMDLLPath="%FastMM%\FullDebugMode DLL\Precompiled\"%FastMMDLL%
  if exist %FastMMDLLPath% goto :copyOK
  goto :copyError

:copyOK
  call :do copy %FastMMDLLPath% %3
  goto :exit

:copyError
:: MUST be the last statement in the batch file
:: this will fail, so Delphi will indicate an error
  copy %2\FastMM_FullDebugMode*.dll %temp%